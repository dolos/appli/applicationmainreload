package com.dolos.application.controllers;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import com.dolos.application.ApplicationMain;
import com.dolos.application.Lang;

import fr.dolos.app.DolosCore;
import fr.dolos.sdk.Core;
import fr.dolos.sdk.UIManager;
import fr.dolos.sdk.models.Drone;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * Handle Dolos main window
 * @author Mathias Hyrondelle
 */
public class MainWindowController implements UIManager, Runnable {

	//Menu allowing enabling / disabling modules
	@FXML
	public Menu menu_modules;
	// Tab panel containing tabs of enabled modules
	@FXML
	public TabPane modules_tabs;
	// Anchor containing tabs, used to bind size
	@FXML
	public AnchorPane tab_anchor;

	private DolosCore core;
	private Thread coreThread;
	private Callable<Void> droneListUpdater = new Callable<Void>() {
		@Override
		public Void call() throws InterruptedException, ExecutionException
		{
			CompletableFuture<Void> future = new CompletableFuture<>();
			ApplicationMain.Instance().getAPI().fetchDrones(drones -> {
				List<Drone> newDrones = drones.stream()
						.filter(drone -> Core.getInstance().getDroneList().filtered((item) -> (item.getIp().equals(drone.getIp()) && item.getPort() == drone.getPort())).isEmpty())
						.collect(Collectors.toList());
				newDrones.forEach(item -> item.getConnection());
				future.complete(null);
			}, t -> {
				System.out.println("An error occured while updating drone list: ");
				t.printStackTrace();
				future.complete(null);
			});
			Core.getInstance().getScheduler().runDelayed(this, 5000);
			return future.get();
		}
	};

	public MainWindowController() {}

	/**
	 * Called upon successfull auth to allow load of everything
	 */
	public void init()
	{	
		System.out.println("Mainwindow init");
		// Create drone tab before loading modules, to ensure it is first
		Tab tab = createTab(Lang.TAB_DRONE_LABEL.get());
		// Start Core
		core = new DolosCore(this);
		Core.setInstance(core);
		core.init();
		coreThread = new Thread(MainWindowController.this);
		coreThread.start();
		FXMLLoader fxmlLoader = new FXMLLoader(MainWindowController.this.getClass().getResource("/fxml/DroneTab.fxml"));
		try {
			AnchorPane content = fxmlLoader.load();
			((DroneTabController) fxmlLoader.getController()).init();
			tab.setContent(content);
			this.bindWidthAndEight(content);
			core.getScheduler().runDelayed(droneListUpdater, 3000);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

	@Override
	public Tab createTab(String moduleName) {
		Tab tab = new Tab(moduleName);
		modules_tabs.getTabs().add(tab);
		CheckMenuItem checkMenu = new CheckMenuItem(moduleName);
		checkMenu.setSelected(true);
		checkMenu.selectedProperty().addListener((observable, old, newValue) -> {
			if (!newValue)
				modules_tabs.getTabs().remove(tab);
			else
				modules_tabs.getTabs().add(tab);
		});
		menu_modules.getItems().add(checkMenu);
		return tab;
	}

	/**
	 * Called by dedicated Core thread, started after fetching drones list in init function
	 */
	@Override
	public void run() {
		core.loop();
		core.release();
	}

	/**
	 * Clean way to stop core loop and wait for it to release ressources
	 */
	public void stopLoop()
	{
		core.stopLoop();
		try {
			if (coreThread.isAlive())
				coreThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void bindWidthAndEight(Pane pane) {
		pane.prefWidthProperty().bind(tab_anchor.widthProperty());
		pane.prefHeightProperty().bind(tab_anchor.heightProperty());
	}
}
