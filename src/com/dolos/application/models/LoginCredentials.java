package com.dolos.application.models;

/**
 * Contain login credentials for serialization with JSOn
 * @author Mathias Hyrondelle
 */
public class LoginCredentials {

	public String email, password;
	public LoginCredentials(String email, String password)
	{
		this.email = email;
		this.password = password;
	}
}
