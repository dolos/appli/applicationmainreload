package com.dolos.application.models;

import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.models.Position;

/**
 * SDK drone implementation with app specific functions
 * @author Mathias Hyrondelle
 */
public class DroneImplem extends Drone {
	
	public DroneImplem(int id, String ip, int port)
	{
		super(id, ip, port);
	}
	
	public DroneImplem(int id, String ip, int port, Position p)
	{
		super(id, ip, port, p);
	}
	
	public DroneImplem(final JSONDrone json) {
		super(json.id, json.ip, json.port);
	}

	public String describe()
	{
		return "ID: " + getId() + ", ip: " + getIp() + ":" + getPort();
	}
}
