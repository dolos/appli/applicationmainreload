module com.dolos.application {
    requires transitive javafx.controls;
    requires transitive javafx.web;
    requires transitive retrofit2;
    requires transitive retrofit2.converter.gson;
    requires java.logging;
    requires transitive applicationlib;
    requires transitive applicationsdk;
    requires transitive java.sql;
    requires transitive com.sothawo.mapjfx;
    requires transitive javafx.fxml;
	requires javafx.graphics;
    
    exports com.dolos.application.controllers;
    
    opens com.dolos.application;
}