package com.dolos.application.api;

import java.util.ArrayList;
import java.util.List;

import com.dolos.application.models.DroneImplem;
import com.dolos.application.models.JSONDrone;
import com.dolos.application.models.JSONDrone.DroneList;
import com.dolos.application.models.LoginCredentials;
import com.dolos.application.models.LoginToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Implementation of DolosAPI using Retrofit as REST client lib
 * @author Mathias Hyrondelle
 */
public class RetrofitService implements DolosAPI {
	private static final String API_URL = "http://0.0.0.0:30000";
	
	private final DolosService api;
	private String token;

	public RetrofitService() {
		Retrofit builder = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
		api = builder.create(DolosService.class);
	}

	@Override
	public void login(LoginCredentials credentials, Runnable onSuccess, APICallback<Throwable> onError) {
		api.loginUser(credentials).enqueue(new Callback<LoginToken>() {
			
			@Override
			public void onResponse(Call<LoginToken> call, Response<LoginToken> response) {
				if (!response.isSuccessful() || response.body() == null)
				{
					onError.run(new RuntimeException("Unable to login"));
					return;
				}
				token = String.format("Bearer %s", response.body().token);
				System.out.println("Successfully logged in, token : " + token);
				onSuccess.run();
			}
			
			@Override
			public void onFailure(Call<LoginToken> call, Throwable t) {
				onError.run(t);
			}
		});
	}

	@Override
	public void fetchDrones(APICallback<List<DroneImplem>> onSuccess, APICallback<Throwable> onError) {
		api.listAvailableDrones(token, 2, 30).enqueue(new Callback<JSONDrone.DroneList>() {

			@Override
			public void onResponse(Call<DroneList> call, Response<DroneList> response) {
				if (!response.isSuccessful() || response.body() == null)
				{
					onError.run(new RuntimeException("Unable to fetch drones"));
					return;
				};
				ArrayList<DroneImplem> ret = new ArrayList<>();
				response.body().drones.forEach(item -> ret.add(item.drone()));
				onSuccess.run(ret);
			}

			@Override
			public void onFailure(Call<DroneList> call, Throwable t) {
				onError.run(t);
			}
		});
	}
	
}
