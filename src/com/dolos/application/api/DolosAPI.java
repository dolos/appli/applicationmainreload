package com.dolos.application.api;

import java.util.List;

import com.dolos.application.models.DroneImplem;
import com.dolos.application.models.LoginCredentials;

/**
 * Define API contact service
 * @author Mathias Hyrondelle
 */
public interface DolosAPI {
	
	public interface APICallback<T>
	{
		public void run(T arg);
	}

	/**
	 * Login user
	 * @param credentials
	 * @param onSuccess Called upon successful authentication
	 * @param onError
	 */
	public void login(LoginCredentials credentials, Runnable onSuccess, APICallback<Throwable> onError);
	/**
	 * Fetch list of drones
	 * @param onSuccess Called upon successfull fetch
	 * @param onError
	 */
	public void fetchDrones(APICallback<List<DroneImplem>> onSuccess, APICallback<Throwable> onError);
}
