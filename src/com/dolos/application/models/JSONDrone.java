package com.dolos.application.models;

import java.util.List;

/**
 * Used for JSON deserialization
 * @author Mathias Hyrondelle
 */
public class JSONDrone {
	
	public int id;
    public String ip;
    public int port;
    
    public JSONDrone() {};
    public JSONDrone(int id, String ip, int port)
    {
    	this.id = id;
    	this.ip = ip;
    	this.port = port;
    }
	
	public void describe()
	{
		System.out.println("ID: " + id + ", ip = " + ip + " port = " + port);
	}
	
	public DroneImplem drone()
	{
		return new DroneImplem(this);
	}
	
	public static class DroneList {
		public List<JSONDrone> drones;
	}

}
