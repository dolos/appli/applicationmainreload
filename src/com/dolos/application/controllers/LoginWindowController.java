package com.dolos.application.controllers;

import com.dolos.application.ApplicationMain;
import com.dolos.application.models.LoginCredentials;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Handle login window
 * @author Mathias Hyrondelle
 */
public class LoginWindowController {

	@FXML
	public TextField userTextField;
	@FXML
	public PasswordField passwordField;
	@FXML
	public Button loginButton;
	private volatile boolean login = false;

	public LoginWindowController() {

	}

	@FXML
	public void onLoginButton(Event event) {
		if (login) return; // Prevent multiple auth
		loginButton.setDisable(true);
		System.out.println("Logging in");
		login = true;
		LoginCredentials credentials = new LoginCredentials(userTextField.getText(), passwordField.getText());
		ApplicationMain.Instance().getAPI().login(credentials, () -> {
			System.out.println("Calling mainWindow init");
			Platform.runLater(() -> {
				ApplicationMain.Instance().getLoginStage().close();
				ApplicationMain.Instance().getMainStage().show();
				ApplicationMain.Instance().getMainWindow().init();
				login = false;
			});
		}, (t) -> {
			System.out.println("Error while logging in:");
			t.printStackTrace();
			loginButton.setDisable(false);
			login = false;
		});
	}
}
