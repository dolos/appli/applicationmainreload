package com.dolos.application.api;

import com.dolos.application.models.JSONDrone;
import com.dolos.application.models.LoginCredentials;
import com.dolos.application.models.LoginToken;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface DolosService {
	/**
     * Login the user
     * @param body ResponseToken.CallToken containing login information
     * @return Token to use for authentication
     */
    @POST("/users/login")
    Call<LoginToken> loginUser(@Body LoginCredentials body);
    /**
     * List all available drones
     * @param token Authentication token
     * @param page Page to fetch
     * @return Drone.DroneList containing the drones data
     */
    @GET("/users/list_drones")
    Call<JSONDrone.DroneList> listAvailableDrones(@Header("Authorization") String token, @Query("page") int page, @Query("size") int size);
}
