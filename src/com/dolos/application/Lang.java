package com.dolos.application;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * Easy access to lang.xml content
 * @author Mathias Hyrondelle
 */
public enum Lang {
	APP_TITLE("app_title", null),
	USERNAME_LABEL("username_label", null),
	PASSWORD_LABEL("password_label", null),
	TAB_DRONE_LABEL("tab_drone_label", null);

	private final String label;
	private final String[] placeholders;

	private Lang(final String label, final String[] placeholders)
	{
		this.label = label;
		this.placeholders = placeholders;
	}

	public String get(final String ...args)
	{
		final Document lang = ApplicationMain.Instance().getLang();
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			Node node = (Node) xPath.evaluate("/string-table/"+label+"/en", lang, XPathConstants.NODE);
			String value = node.getTextContent();
			if (placeholders == null) return value;
			int i = 0;
			for (String s : placeholders)
			{
				if (args[i] == null) continue;
				value = value.replaceAll("\\["+ s + "\\]", args[i]);
				i += 1;
			}
			return value;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
	}
}
