package com.dolos.application.models;

/**
 * Contains the response token from a login call
 * @author Mathias Hyrondelle
 */
public class LoginToken {
	public String token;
	
	public LoginToken() {}
	public LoginToken(String token) { this.token = token; }
}
