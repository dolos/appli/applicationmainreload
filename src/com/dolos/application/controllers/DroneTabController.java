package com.dolos.application.controllers;

import com.sothawo.mapjfx.Coordinate;
import com.sothawo.mapjfx.MapLabel;
import com.sothawo.mapjfx.MapType;
import com.sothawo.mapjfx.MapView;
import com.sothawo.mapjfx.Marker;
import com.sothawo.mapjfx.Marker.Provided;
import com.sothawo.mapjfx.event.MapViewEvent;

import fr.dolos.app.commands.AddWaypointCmdHandler;
import fr.dolos.sdk.Core;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.models.Position;
import fr.dolos.sdk.network.NetworkClient;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/**
 * Handle drones tab
 * @author Mathias Hyrondelle
 */
public class DroneTabController {

	@FXML
	public ListView<Drone> drone_list;
	@FXML
	public MapView mapView;
	@FXML
	public BorderPane border_pane;
	@FXML
	public AnchorPane anchor_pane;

	/**
	 * Create map marker for drone
	 * @param drone
	 */
	private void createDroneMarker(Drone drone)
	{
		MapLabel markerName = new MapLabel(drone.getIp()).setVisible(true);
		Coordinate markerPos = new Coordinate(drone.getPosition().latitude, drone.getPosition().longitude);
		Marker marker = Marker.createProvided(Provided.GREEN).attachLabel(markerName).setVisible(true).setPosition(markerPos);

		// Bind marker pos to drone pos
		drone.positionProperty().addListener((property, oldPos, newPos) -> {
			Coordinate markerPosNew = new Coordinate(newPos.latitude, newPos.longitude);
			Platform.runLater(() -> marker.setPosition(markerPosNew)); 
		});
		Platform.runLater(() -> mapView.addMarker(marker));
		drone.getData().put("map_marker", marker);
	}

	private void setMarkerDest(final Drone drone, final Coordinate coord)
	{
		Marker droneMarker = (Marker) drone.getData().get("dest_marker");
		if (droneMarker == null)
		{
			MapLabel markerName = new MapLabel(drone.getIp()).setVisible(true);
			final Marker add = Marker.createProvided(Provided.ORANGE).attachLabel(markerName).setVisible(true).setPosition(coord);
			if (Platform.isFxApplicationThread())
				mapView.addMarker(add);
			else
				Platform.runLater(() -> mapView.addMarker(add));
			drone.getData().put("dest_marker", add);
			return;
		}
		if (Platform.isFxApplicationThread())
			droneMarker.setPosition(coord);
		else
			Platform.runLater(() -> droneMarker.setPosition(coord));
	}

	/**
	 * Init MapView, bind drones position to their markers
	 */
	private void initMapAndControls()
	{
		// After map is initialized
		mapView.initializedProperty().addListener((observable, old, newVal) -> {
			if (!newVal) return;
			Coordinate _mapcenter = new Coordinate(48.866494, 2.373565); // Parmentier
			mapView.setCenter(_mapcenter);
			mapView.setZoom(14);

			// Create map markers on drones
			Core.getInstance().getDroneList().forEach(drone -> createDroneMarker(drone));
			drone_list.getItems().addListener(new ListChangeListener<Drone>() {

				@Override
				public void onChanged(Change<? extends Drone> c) {
					while (c.next())
					{
						c.getAddedSubList().forEach(drone -> createDroneMarker(drone));
						c.getRemoved().forEach(drone -> {
							final Marker marker = (Marker) drone.getData().get("map_marker");
							final Marker destMarker = (Marker) drone.getData().get("dest_marker");
							if (marker == null) return;
							Platform.runLater(() -> {
								mapView.removeMarker(marker);
								if (destMarker != null)
									mapView.removeMarker(destMarker);
							});
						});
					}
				}

			});
		});

		// Handle MAP CLICKED event
		mapView.addEventHandler(MapViewEvent.MAP_CLICKED, event -> {
			event.consume();
			final Coordinate coord = event.getCoordinate().normalize();
			final Drone drone = drone_list.getSelectionModel().getSelectedItem();
			if (drone == null)
				return;
			final AddWaypointCmdHandler.WaypointPacket packet = new AddWaypointCmdHandler.WaypointPacket(coord.getLatitude(), coord.getLongitude(), 30);
			Core.getInstance().getScheduler().runAsync(() -> {
				System.out.println("Sending packet");
				NetworkClient client = drone.getConnection();
				if (client == null)
					throw new RuntimeException("Connection to drone is not established");
				setMarkerDest(drone, coord);
				drone.getConnection().send(packet);
				return null;
			});
		});

		// Now initialize map
		mapView.setMapType(MapType.BINGMAPS_AERIAL);
		mapView.initialize();
	}

	/**
	 * Set the drone as the map center
	 * @param old Old drone if there is one, used to remove the listener
	 * @param newDrone New drone to follow
	 */
	private void updateMapCenter(Drone old, Drone newDrone)
	{
		if (old != null)
		{
			@SuppressWarnings("unchecked")
			ChangeListener<? super Position> listener = (ChangeListener<? super Position>) old.getData().get("map_listener");
			old.positionProperty().removeListener(listener);
			old.getData().remove("map_listener");
		}

		if (newDrone != null)
		{
			ChangeListener<Position> listener = new ChangeListener<Position>() {

				@Override
				public void changed(ObservableValue<? extends Position> observable, Position oldValue, Position newValue) {
					final Coordinate coord = new Coordinate(newValue.latitude, newValue.longitude);
					Platform.runLater(() -> mapView.setCenter(coord));
				}
			};
			newDrone.positionProperty().addListener(listener);
			newDrone.getData().put("map_listener", listener);
		}
	}

	/**
	 * Setup drone list binding, auto resize and values
	 */
	private void setupDroneList()
	{
		// Cell factory to display drone image and ip
		drone_list.setCellFactory(list -> {
			ListCell<Drone> cell = new ListCell<Drone>() {
				@Override
				protected void updateItem(Drone item, boolean empty) {
					if (empty || item == null)
					{
						setText(null);
						setGraphic(null);
						return;
					}
					setGraphic(new ImageView(getClass().getResource("/assets/images/drone_image.png").toString()));
					setText(item.getIp());
				};
			};
			// Handle when we change selection
			// Bind currently selected drone position to map center
			cell.setOnMouseClicked(e -> {
				Drone old = drone_list.getSelectionModel().getSelectedItem();
				drone_list.getSelectionModel().select(cell.getIndex());
				updateMapCenter(old, drone_list.getSelectionModel().getSelectedItem());
			});
			return cell;
		});

		Bindings.bindContent(drone_list.getItems(), Core.getInstance().getDroneList());
		if (drone_list.getItems().size() > 0)
			drone_list.getSelectionModel().select(0);
		drone_list.prefHeightProperty().bind(anchor_pane.heightProperty());
		updateMapCenter(null, drone_list.getSelectionModel().getSelectedItem());
	}

	public void init()
	{
		// Border pane containing list and mapview
		// This allow automatic resize
		border_pane.prefHeightProperty().bind(anchor_pane.heightProperty());
		border_pane.prefWidthProperty().bind(anchor_pane.widthProperty());
		this.initMapAndControls();
		this.setupDroneList();

		System.out.println("Out init");
	}
}
