package com.dolos.application;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import com.dolos.application.api.DolosAPI;
import com.dolos.application.api.RetrofitService;
import com.dolos.application.controllers.MainWindowController;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Handle main
 * @author Mathias Hyrondelle
 */
public class ApplicationMain extends Application {
	
	private static ApplicationMain instance;
	
	public static ApplicationMain Instance()
	{
		return instance;
	}
	
	private Document lang;
	private DolosAPI api;
	private Stage mainStage;
	private Stage loginStage;
	private MainWindowController mainWindow;

	/**
	 * Used to launch the JavaFX App
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage loginStage) throws Exception {
		ApplicationMain.instance = this;
		this.loginStage = loginStage;
		System.out.println("Setting up API service");
		this.api = new RetrofitService();
		
		System.out.println("Loading lang.xml");
		InputStream lang = this.getClass().getResourceAsStream("/lang.xml");
		this.lang = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(lang);

		System.out.println("Loading Login.fxml");
		BorderPane box = new FXMLLoader(this.getClass().getResource("/fxml/Login.fxml")).load();
		
		System.out.println("Loading MainWindow.fxml");
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/MainWindow.fxml"));
		VBox vbox = loader.load();
		mainWindow = loader.getController();
		
		// Preload main window fxml
		System.out.println("Loading main window");
		mainStage = new Stage();
		mainStage.setScene(new Scene(vbox));
		mainStage.setTitle(Lang.APP_TITLE.get());
		mainStage.setOnCloseRequest(windowEvent -> {
			mainWindow.stopLoop();
			Platform.exit();
			System.exit(0);
		});
		
		System.out.println("Loading login window");
		loginStage.setScene(new Scene(box));
		loginStage.setTitle(Lang.APP_TITLE.get());
		loginStage.show();
		
		System.out.println("Done init");
	}

	public Document getLang()
	{
		return lang;
	}
	
	public DolosAPI getAPI()
	{
		return api;
	}
	
	public Stage getMainStage()
	{
		return mainStage;
	}
	
	public Stage getLoginStage()
	{
		return loginStage;
	}
	
	public MainWindowController getMainWindow()
	{
		return mainWindow;
	}
}
